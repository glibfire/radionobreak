﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Harmony;

namespace RadioNoBreak
{
    public class MainPatcher
    {
        public static void Patch()
        {
            var harmony = HarmonyInstance.Create("com.oldark.subnautica.radionobreak.mod");
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }

    }
}
