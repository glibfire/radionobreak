﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Harmony;

namespace RadioNoBreak
{
    [HarmonyPatch(typeof(EscapePod))]
    [HarmonyPatch("DamageRadio")]

    //This should save the radio if I'm understanding correctly.
    internal class EscapePod_DamageRadio_Patcher
    {
        [HarmonyPrefix]
        public static bool Prefix()
        {
            return false;
        }
    }
}
